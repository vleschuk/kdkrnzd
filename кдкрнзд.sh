#!/bin/bash
DATE=`date +%d.%m.%Y`
[ -n  "$1" ] && DATE=$1
curl https://www.cbr.ru/currency_base/daily.aspx?date_req=${DATE} 2>/dev/null \
       |grep USD -A 3|tail -1|grep -E -o '[0-9]+,[0-9]+'|tr , .
